exports.categories = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var breadcrumbs = [ req.params.topCategory ];

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');

		var query = { name: req.params.topCategory };
		collection.findOne(query, function(err, category){
			res.render("categories", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : category.page_title,
				topCategory : category,
				breadcrumbs: breadcrumbs
			});

			db.close();
		});
	});
};

exports.products = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var breadcrumbs = [ req.params.topCategory, req.params.subCategory ];

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var productsCollection = db.collection('products');
			var query = { primary_category_id: new RegExp("^"+req.params.topCategory+"-"+req.params.subCategory,"i")};
			productsCollection.find(query).toArray(function(err, products) {
				res.render("products", { 
					// Underscore.js lib
					_     : _, 
					
					// Template data
					title : "Details Page!",
					products : products,
					detailsPath : "/" + req.params.topCategory + "/subcategory/" + req.params.subCategory + "/details",
					breadcrumbs: breadcrumbs
				});

			db.close();
		});
	});
};

exports.details = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		var query = { id: req.params.itemId};
		collection.findOne(query, function(err, product){
			var breadcrumbs = [ req.params.topCategory, req.params.subCategory, product.name ];
			res.render("details", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Details Page!",
				item : product,
				breadcrumbs: breadcrumbs
			});

			db.close();
		});
	});
};